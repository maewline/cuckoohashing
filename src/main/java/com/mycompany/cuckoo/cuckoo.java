/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cuckoo;

/**
 *
 * @author informatics
 */
public class Cuckoo {

    private int key;
    private String value;
    private Cuckoo[] lookupTable1 = new Cuckoo[97]; // assume size = 97  
    private Cuckoo[] lookupTable2 = new Cuckoo[97]; // assume size = 97  

    public Cuckoo(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public Cuckoo() {

    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public int hash1(int key) {
        return key % lookupTable1.length;
    }

    public int hash2(int key) {
        return (key / 97) % lookupTable2.length;
    }

    public void put(int key, String value) {

        if (lookupTable1[hash1(key)] != null && lookupTable1[hash1(key)].key == key) {
            lookupTable1[hash1(key)].key = key;
            lookupTable1[hash1(key)].value = value;
            return;
        }
        if (lookupTable2[hash2(key)] != null && lookupTable2[hash2(key)].key == key) {
            lookupTable2[hash2(key)].key = key;
            lookupTable2[hash2(key)].value = value;
            return;
        }
        int i = 0;

        //repeat
        while (true) {
            if (i == 0) { //table 1
                if (lookupTable1[hash1(key)] == null) {
                    lookupTable1[hash1(key)] = new Cuckoo();
                    lookupTable1[hash1(key)].key = key;
                    lookupTable1[hash1(key)].value = value;
                    return;
                }
            } else { //table 2 (i==1)
                if (lookupTable2[hash2(key)] == null) {
                    lookupTable2[hash2(key)] = new Cuckoo();
                    lookupTable2[hash2(key)].key = key;
                    lookupTable2[hash2(key)].value = value;
                    return;
                }
            }

            //eviction
            if (i == 0) { //table 1
                String tempValue = lookupTable1[hash1(key)].value;
                int tempKey = lookupTable1[hash1(key)].key;
                lookupTable1[hash1(key)].key = key;
                lookupTable1[hash1(key)].value = value;
                key = tempKey;
                value = tempValue;

            } else { //table 2 (i==1)
                String tempValue = lookupTable2[hash2(key)].value;
                int tempKey = lookupTable2[hash2(key)].key;
                lookupTable2[hash2(key)].key = key;
                lookupTable2[hash2(key)].value = value;
                key = tempKey;
                value = tempValue;
            }
            i = (i + 1) % 2;
        }
    }

    public Cuckoo get(int key) {
        if (lookupTable1[hash1(key)] != null && lookupTable1[hash1(key)].key == key) {
            return lookupTable1[hash1(key)];
        }
        if (lookupTable2[hash2(key)] != null && lookupTable2[hash2(key)].key == key) {
            return lookupTable2[hash2(key)];
        }
        return null;
    }

    public Cuckoo delete(int key) {
        if (lookupTable1[hash1(key)] != null && lookupTable1[hash1(key)].key == key) {
            Cuckoo temp = lookupTable1[hash1(key)];
            lookupTable1[hash1(key)] = null;
            return temp;
        }
        if (lookupTable2[hash2(key)] != null && lookupTable2[hash2(key)].key == key) {
            Cuckoo temp = lookupTable2[hash2(key)];
            lookupTable2[hash2(key)] = null;
            return temp;
        }
        return null;
    }

    @Override
    public String toString() {
        return "Key: " + key + " Value: " + value;
    }

}
