/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.cuckoo;

/**
 *
 * @author Arthi
 */
public class testCuckoo {

    public static void main(String[] args) {
        Cuckoo cuckoo = new Cuckoo();
        cuckoo.put(100, "Maewline");
        cuckoo.put(3, "Arthit");
        cuckoo.put(101, "Sakda");
        cuckoo.put(198, "Aek");
        System.out.println(cuckoo.get(100).toString());
        System.out.println(cuckoo.get(3).toString());
        System.out.println(cuckoo.get(101).toString());
        System.out.println(cuckoo.get(198).toString());
        cuckoo.delete(3);
        System.out.println(cuckoo.get(3));
    }
}
